# staticwiki

Yet another low-effort tool for rendering MediaWiki XML dumps to static html
pages.

## Features

- partial wikitext support
- can accept bzip2 compressed streams.
- can output tar stream (could be piped to mksquashfs to recompress without
  writing to disk)
- horribly bad code :)

## Usage

- clone repo
- obtain rustc (tested with nightly)
- `cargo install --path .`
- Get help: `staticwiki --help`
- Convert to squashfs:
  `staticwiki --bzip2 --tar < dump.xml.bz2 | mksquashfs - wiki.sfs -tar -comp zstd`

## License

`AGPL-3.0-only`
