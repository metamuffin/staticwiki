use clap::Parser;
use parse_wiki_text::{Node, Parameter};
use std::fs::File;
use std::io::{stdin, stdout, Read, Write};
use tar::Header;

extern crate bzip2;
extern crate parse_mediawiki_dump;

#[derive(Parser)]
struct Args {
    /// Stop after n articles (for debugging)
    #[arg(short, long)]
    limit: Option<usize>,
    /// Read bzip2 compressed input
    #[arg(short, long)]
    bzip2: bool,
    /// Stream tar to stdout
    #[arg(short, long)]
    tar: bool,
    /// Show non-fatal warnings    
    #[arg(short, long)]
    verbose: bool,
    #[arg(short, long, default_value = "")]
    footer: String,
}

fn main() {
    let args = Args::parse();

    let mut input: Box<dyn Read> = Box::new(stdin());
    if args.bzip2 {
        input = Box::new(bzip2::read::BzDecoder::new(input))
    }

    let input = std::io::BufReader::new(input);
    let mut archive = tar::Builder::new(stdout());

    let footer = format!(
        "<a href=\"https://codeberg.org/metamuffin/staticwiki\">staticwiki</a>; {}",
        args.footer
    );

    for (i, result) in parse_mediawiki_dump::parse(input).enumerate() {
        match result {
            Err(error) => {
                eprintln!("xml format error: {}", error);
                break;
            }
            Ok(page) => {
                if page.namespace == 0
                    && match &page.format {
                        None => false,
                        Some(format) => format == "text/x-wiki",
                    }
                    && match &page.model {
                        None => false,
                        Some(model) => model == "wikitext",
                    }
                {
                    let filename = urlencode(&page.title);

                    let ast = parse_wiki_text::Configuration::default().parse(&page.text);
                    if args.verbose {
                        for w in ast.warnings {
                            eprintln!("wikitext warning: {}", w.message.message())
                        }
                    }
                    use std::fmt::Write;
                    let mut refs = vec![];
                    let mut html = String::from("<!DOCTYPE html><html><head>");
                    write!(&mut html, "<title>{}</title></head>", escape(&page.title)).unwrap();
                    write!(&mut html, "<body><h1>{}</h1>", escape(&page.title)).unwrap();
                    render_toc(&mut html, &ast.nodes);
                    render_nodes(&mut html, &mut refs, &ast.nodes);
                    render_refs(&mut html, &refs);
                    write!(&mut html, "<footer>{footer}</footer>").unwrap();
                    write!(&mut html, "</body></html>").unwrap();

                    if args.tar {
                        let mut header = Header::new_gnu();
                        header.set_size(html.as_bytes().len() as u64);
                        header.set_cksum();
                        archive
                            .append_data(&mut header, filename, html.as_bytes())
                            .unwrap();
                    } else {
                        let mut f = File::create(format!("out/{}", filename)).unwrap();
                        f.write_all(html.as_bytes()).unwrap()
                    }
                } else {
                    eprintln!("page ignored: {:?}", page.title);
                }
            }
        }
        if Some(i) == args.limit {
            break;
        }
    }
    archive.finish().unwrap();
}

fn urlencode(t: &str) -> String {
    t.replace("/", "_")
}
pub fn escape(text: &str) -> String {
    text.replace("&", "&amp;")
        .replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("'", "&#8217;")
        .replace("\"", "&quot;")
}

fn render_nodes(html: &mut String, refs: &mut Vec<String>, nodes: &Vec<Node>) {
    for n in nodes {
        render_node(html, refs, n)
    }
}

fn render_nodes_to_string(nodes: &Vec<Node>, refs: &mut Vec<String>) -> String {
    let mut html = String::new();
    render_nodes(&mut html, refs, nodes);
    return html;
}

fn render_node(html: &mut String, refs: &mut Vec<String>, n: &Node) {
    use std::fmt::Write;
    match n {
        parse_wiki_text::Node::Bold { .. } => (),
        parse_wiki_text::Node::BoldItalic { .. } => (),
        parse_wiki_text::Node::Category {
            ordinal: _,
            target: _,
            ..
        } => write!(html, "[todo]").unwrap(),
        parse_wiki_text::Node::CharacterEntity { character: _, .. } => {
            write!(html, "[todo: character]").unwrap()
        }
        parse_wiki_text::Node::Comment { .. } => (),
        parse_wiki_text::Node::DefinitionList { items: _, .. } => {
            write!(html, "[todo: def list]").unwrap()
        }
        parse_wiki_text::Node::EndTag { name: _, .. } => write!(html, "[todo: tag end]").unwrap(),
        parse_wiki_text::Node::ExternalLink { nodes: _, .. } => {
            write!(html, "[todo: external link]").unwrap()
        }
        parse_wiki_text::Node::Heading { level, nodes, .. } => {
            let h = render_nodes_to_string(nodes, refs);
            write!(html, "<h{level} id=\"{}\">{}</h{level}>", urlencode(&h), h).unwrap();
        }
        parse_wiki_text::Node::HorizontalDivider { .. } => write!(html, "<hr>").unwrap(),
        parse_wiki_text::Node::Image {
            target: _, text: _, ..
        } => write!(html, "[todo: image]").unwrap(),
        parse_wiki_text::Node::Italic { .. } => (),
        parse_wiki_text::Node::Link { target, text, .. } => write!(
            html,
            "<a href=\"{}\">{}</a>",
            urlencode(target), // TODO does this always link to wikipedia?
            render_nodes_to_string(text, refs)
        )
        .unwrap(),
        parse_wiki_text::Node::MagicWord { .. } => write!(html, "[todo: magic]").unwrap(),
        parse_wiki_text::Node::OrderedList { items, .. } => write!(
            html,
            "<ol>{}</ol>",
            items
                .iter()
                .map(|e| format!("<li>{}</li>", render_nodes_to_string(&e.nodes, refs)))
                .collect::<Vec<_>>()
                .join("")
        )
        .unwrap(),
        parse_wiki_text::Node::UnorderedList { items, .. } => write!(
            html,
            "<ul>{}</ul>",
            items
                .iter()
                .map(|e| format!("<li>{}</li>", render_nodes_to_string(&e.nodes, refs)))
                .collect::<Vec<_>>()
                .join("")
        )
        .unwrap(),
        parse_wiki_text::Node::ParagraphBreak { .. } => write!(html, "</p><p>").unwrap(),
        parse_wiki_text::Node::Parameter {
            default: _,
            name: _,
            ..
        } => write!(html, "[todo: parameter]").unwrap(),
        parse_wiki_text::Node::Preformatted { nodes, .. } => {
            write!(html, "<pre>{}</pre>", render_nodes_to_string(nodes, refs)).unwrap()
        }
        parse_wiki_text::Node::Redirect { target, .. } => write!(
            html,
            "Redirect: <a href=\"{}\">{}</a>",
            urlencode(target),
            urlencode(target)
        )
        .unwrap(),
        parse_wiki_text::Node::StartTag { name: _, .. } => {
            write!(html, "[todo: start tag]").unwrap()
        }
        parse_wiki_text::Node::Table {
            attributes: _,
            captions: _,
            rows: _,
            ..
        } => write!(html, "[todo: table]").unwrap(),
        parse_wiki_text::Node::Tag { name, nodes, .. } => match name.as_ref() {
            "ref" => {
                if !nodes.is_empty() {
                    let r = render_nodes_to_string(nodes, refs);
                    refs.push(r);
                    let refid = refs.len();
                    write!(html, "<sup><a href=\"#{}\">[{}]</a></sup>", refid, refid).unwrap();
                }
            }
            _ => write!(html, "[todo: {name:?} tag]").unwrap(),
        },
        parse_wiki_text::Node::Template {
            name, parameters, ..
        } => {
            let name = match name.first() {
                Some(Node::Text { value, .. }) => value,
                _ => "",
            };
            render_template(html, refs, name, parameters);
        }
        parse_wiki_text::Node::Text { value, .. } => write!(html, "{}", escape(value)).unwrap(),
    }
}

fn render_toc(html: &mut String, nodes: &Vec<Node>) {
    use std::fmt::Write;
    write!(html, "<div><h4><i>Table of contents</i></h4>").unwrap();
    let mut k = 0;
    for n in nodes {
        match n {
            Node::Heading { level, nodes, .. } => {
                let level = *level as usize;
                while k < level {
                    k += 1;
                    write!(html, "<ol>").unwrap();
                }
                while k > level {
                    k -= 1;
                    write!(html, "</ol>").unwrap();
                }
                let h = render_nodes_to_string(nodes, &mut vec![]);

                write!(html, "<li><a href=\"#{}\">{}</a></li>", urlencode(&h), h).unwrap();
            }
            _ => (),
        }
    }
    write!(html, "</div>").unwrap();
}

pub fn render_template(
    html: &mut String,
    refs: &mut Vec<String>,
    name: &str,
    params: &Vec<Parameter>,
) -> Option<()> {
    use std::fmt::Write;
    match name {
        // TODO this can panic
        "lang" => write!(
            html,
            "{}",
            render_nodes_to_string(&params.get(1)?.value, refs)
        )
        .unwrap(),
        "IPA" => write!(
            html,
            "<code>{}</code>",
            render_nodes_to_string(&params.get(0)?.value, refs)
        )
        .unwrap(),

        "Internetquelle" | "Literatur" => {
            write!(html, "{}: <ul>", escape(name)).unwrap();
            for p in params {
                let key = p
                    .name
                    .as_ref()
                    .map(|n| render_nodes_to_string(n, &mut vec![]))
                    .unwrap_or(String::from("??"));
                let value = render_nodes_to_string(&p.value, &mut vec![]);
                if let "url" | "archiv-url" | "Online" = key.as_str() {
                    write!(
                        html,
                        "<li>{}: <a href=\"{}\">{}</a></li>",
                        key, value, value
                    )
                } else {
                    write!(html, "<li>{}: {}</li>", key, value)
                }
                .unwrap()
            }
            write!(html, "</ul>").unwrap();
        }
        "Siehe auch" | "Hauptartikel" => {
            let k = text_node(params.get(0)?.value.get(0)?);
            write!(
                html,
                "<i>{}: <a href=\"{}\">{}</a></i>",
                escape(name),
                urlencode(&k),
                escape(&k)
            )
            .unwrap();
        }

        _ => {
            write!(html, "[todo: {name:?} template]").unwrap();
            // write!(html, "[todo: {name:?} template <pre>{params:#?}</pre>]").unwrap();
            // eprintln!("unsupported template {name:?}");
            // eprintln!("{params:?}");
        }
    }
    Some(())
}

pub fn text_node(n: &Node) -> String {
    match n {
        Node::Text { value, .. } => value.to_string(),
        _ => String::from("[todo: fix this bug :) ]"),
    }
}

fn render_refs(html: &mut String, refs: &Vec<String>) {
    use std::fmt::Write;
    write!(html, "<ol>").unwrap();
    for (i, r) in refs.iter().enumerate() {
        write!(html, "<li id=\"{}\">{r}</li>", i + 1).unwrap()
    }
    write!(html, "</ol>").unwrap();
}
